using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditorInternal;
using Cinemachine;
using StarterAssets;

public class ShooterController : MonoBehaviour
{
    [Header("Camera Stuff")]
    [SerializeField] private Transform mainCamera;
    [SerializeField] private CinemachineVirtualCamera aimVirtualCamera;

    [Header("Input sensitivity configuration")]
    [SerializeField] private float normalSensitivity;
    [SerializeField] private float aimSensitivity;
    [SerializeField] private float normalCameraRotationGunOffset;
    [SerializeField] private float aimCameraRotationGunOffset;

    [Header("Gun transform points")]
    [SerializeField] private Transform normalGunTransform;
    [SerializeField] private Transform aimGunTransform;
    [SerializeField] private Transform storedGunTransform;

    [SerializeField] private Transform guns;

    private FirstPersonController firstPersonController;
    private StarterAssetsInputs starterAssetsInputs;
    private Transform currentGun = null;

    void Start()
    {
        firstPersonController = GetComponent<FirstPersonController>();
        starterAssetsInputs = GetComponent<StarterAssetsInputs>();

        foreach (Transform gun in guns)
        {
            if (currentGun == null)
                currentGun = gun;

            gun.position = storedGunTransform.position;
        }

        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);

        aimVirtualCamera.gameObject.SetActive(starterAssetsInputs.aim);
        firstPersonController.SetSensitivity((starterAssetsInputs.aim) ? aimSensitivity : normalSensitivity);

        Vector3 cameraRotationOffset = new Vector3(0, -firstPersonController.GetCameraPitch() * ((starterAssetsInputs.aim) ? aimCameraRotationGunOffset : normalCameraRotationGunOffset));
        Vector3 gunTargetPosition = ((starterAssetsInputs.aim) ? aimGunTransform.position : normalGunTransform.position) + cameraRotationOffset;
        currentGun.position = Vector3.Lerp(currentGun.transform.position, gunTargetPosition, 100f * Time.deltaTime);
        currentGun.rotation = mainCamera.rotation;

        WeaponController weapon = currentGun.GetComponent<WeaponController>();
        weapon.animator.SetBool("Aiming", starterAssetsInputs.aim);
        if (starterAssetsInputs.shoot)
        {
            weapon.Shoot();
            starterAssetsInputs.shoot = weapon.IsContinuous();
        }
    }
}
