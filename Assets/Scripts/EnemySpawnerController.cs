using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerController : MonoBehaviour
{
    [SerializeField] private List<Transform> enemyPrefabs;
    [SerializeField] private List<int> spawnOrder;
    [SerializeField] private float startDelay = 0f; // Time until the spawner starts working
    [SerializeField] private float initialInterval = 5f; // Initial time between spawns
    [SerializeField] private float intervalFallof = 0.1f; // Percentage the time will be decreased by each spawn

    private float currentInterval;
    private int currentIndex = 0;

    void Start()
    {
        currentInterval = initialInterval;
        Invoke(nameof(Spawn), startDelay + currentInterval);
    }

    void Spawn()
    {
        print("Enemy Spawned");
        initialInterval *= (1 - intervalFallof);
        var newEnemy = Instantiate(enemyPrefabs[spawnOrder[currentIndex]], transform);
        currentIndex = (currentIndex + 1) % spawnOrder.Count;

        Invoke(nameof(Spawn), currentInterval);
    }
}
