using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [SerializeField] private Transform bulletPrefab;
    [SerializeField] private Transform bulletOrigin;
    [SerializeField] private LayerMask colliderMask;

    [SerializeField] private bool continuosShooting = false;
    [SerializeField] private float baseDamage = 20f;

    public Animator animator;

    void Awake()
    {
        animator = this.GetComponent<Animator>();
    }

    public void Shoot()
    {
        // Only shoot if the gun is not playing the shoot animation
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Shoot")) return;

        animator.SetTrigger("Shoot");

        Vector3 mouseWorldPosition = Vector3.zero;
        Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
        if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, colliderMask))
            mouseWorldPosition = raycastHit.point;

        Vector3 aimdirection = (mouseWorldPosition - bulletOrigin.position).normalized;
        var bullet = Instantiate(bulletPrefab, bulletOrigin.position, Quaternion.LookRotation(aimdirection, Vector3.up));
        bullet.GetComponent<BulletController>().baseDamage = baseDamage;
    }

    public bool IsContinuous()
    {
        return continuosShooting;
    }
}
