using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private float maxLife = 100f;

    private float life;

    // Start is called before the first frame update
    void Start()
    {
        life = maxLife;
    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
    }

    public void TakeDamage(float damage)
    {
        life -= damage;
        if (life <= 0)
            GetComponent<DisolveEffectController>().disolve = true;
    }
}
