using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private float attackDistance = 2.0f;

    private NavMeshAgent agent;
    private Animator animator;
    private Transform player;

    private bool attacked = false;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player").transform;
    }

    void Update()
    {
        bool isRunning = animator.GetCurrentAnimatorStateInfo(0).IsName("run");
        if (!isRunning) return;

        bool isCloseToPlayer = Vector3.Distance(this.transform.position, player.position) < attackDistance;

        if (!isCloseToPlayer)
        {
            attacked = false;
            agent.SetDestination(player.position);
        }
        else
        {
            agent.SetDestination(this.transform.position);
            if (!attacked)
            {
                animator.SetTrigger("Attack");
                attacked = true;
            }
            else attacked = false;
        }

    }
}
