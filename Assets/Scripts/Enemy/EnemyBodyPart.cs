using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBodyPart : MonoBehaviour
{
    [SerializeField] private float damageMultiplier = 1f;

    private EnemyController controller;

    void Awake()
    {
        controller = this.GetComponentInParent<EnemyController>();
    }

    public void TakeDamage(float baseDamage)
    {
        controller.TakeDamage(baseDamage * damageMultiplier);
    }
}
