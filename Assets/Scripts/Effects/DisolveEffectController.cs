using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisolveEffectController : MonoBehaviour
{
    [SerializeField] private float disolveRate = 0.8f;
    public bool disolve = false;

    private List<Material> materials;

    void Start()
    {
        materials = new List<Material>();
        foreach (var mesh in GetComponentsInChildren<SkinnedMeshRenderer>())
            materials.Add(mesh.material);
    }

    void Update()
    {
        if (disolve)
        {
            foreach (Material material in materials)
            {
                float disolveAmount = material.GetFloat("_DisolveAmount");
                if (disolveAmount < 1)
                    material.SetFloat("_DisolveAmount", disolveAmount + disolveRate * Time.deltaTime);
                else
                    Destroy(gameObject);
            }
        }
    }
}
