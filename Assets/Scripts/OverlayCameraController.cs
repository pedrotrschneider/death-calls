using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlayCameraController : MonoBehaviour
{
    [SerializeField] private Camera overlayCamera;

    private Camera thisCamera;

    void Start()
    {
        thisCamera = GetComponent<Camera>();
    }

    void Update()
    {
        overlayCamera.fieldOfView = thisCamera.fieldOfView;
        overlayCamera.farClipPlane = thisCamera.farClipPlane;
        overlayCamera.nearClipPlane = thisCamera.nearClipPlane;
    }
}
