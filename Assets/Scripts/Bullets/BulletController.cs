using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField] private ParticleSystem particles;
    [SerializeField] private GameObject trail;
    [SerializeField] private GameObject visual;
    [SerializeField] private float speed = 40f;

    private Rigidbody bulletRigidBody;
    private bool hit = false;

    public float baseDamage;

    private void Awake()
    {
        bulletRigidBody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        bulletRigidBody.velocity = transform.forward * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (hit) return;
        hit = true;

        visual.SetActive(false);
        bulletRigidBody.velocity = Vector3.zero;
        var emission = particles.emission;
        emission.rateOverDistance = 0;

        if (other.gameObject.GetComponent<EnemyBodyPart>() != null)
            other.gameObject.GetComponent<EnemyBodyPart>().TakeDamage(baseDamage);

        Invoke(nameof(DestroySelf), 1.5f);
    }

    private void DestroySelf()
    {
        Destroy(gameObject);
    }
}
