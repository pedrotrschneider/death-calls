# Death Calls
## A first-person survival shooter

I am currently woring on this game to study diffent visual effects that can be applied to games, such as projectile and disolving visual effects.

## Screenshots

### Gameplay

![](Screenshots/gameplay-capture.mp4){width=100%}

![](/Screenshots/screenshot1.png)

![](/Screenshots/screenshot3.png)

### Effects

Disolve shader

![](Screenshots/disolve-capture.mp4){width=100%}


Projectile effect

![](Screenshots/projectile-capture.mp4){width=100%}